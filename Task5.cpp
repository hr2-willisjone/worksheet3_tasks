#include <cstdio>
#include <iostream>
#include <string>
using namespace std; 

int main()
{
   for (int i = 1; i < 31; i++)
   {
       if((i%3) == 0)   
       {
           std::cout << "Fizz\n";
       }
       if ((i%5) == 0)
       {
           std::cout << "Buzz\n";
       }
       else
       {
           std::cout << i << "\n";
       }
    }
return 0; 

}      
       
       


// %d = Take the next argument and print it as an int
// \n = A newline/carriage-return character.
// \t = \t' is a horizontal tab, giving tab space horizontally in your output.
// %s = Take the next argument and print it as a string}